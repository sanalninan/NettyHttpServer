package NettyHttpServer;

import org.glassfish.jersey.message.internal.MediaTypeProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sanalninan on 10/31/16.
 */
@Path("/myresource")
public class MyResource {
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getJSONResponse(){
        Map map = new HashMap();
        map.put(1, "234");
        return Response
                .status(Response.Status.OK)
                .entity(map.toString())
                .type(MediaType.APPLICATION_JSON)
                .build();

    }

}
