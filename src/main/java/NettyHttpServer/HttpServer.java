package NettyHttpServer;

import io.netty.channel.Channel;
import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

/**
 * My NIO based HttpServer
 *
 */
public class HttpServer
{
    private static final Logger LOG = LoggerFactory.getLogger(HttpServer.class);
    private static URI BASE_URI = URI.create("http://localhost:8080/");
    private static final String ROOT_PATH = "myresource";

    public static void main( String[] args )
    {
        try{
            ResourceConfig resourceConfig = new ResourceConfig(MyResource.class);
            final Channel server = NettyHttpContainerProvider.createHttp2Server(BASE_URI, resourceConfig, null);
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                          server.close();

                }
            }));
        }catch (Exception exp){
            LOG.error(exp.getMessage());
        }

        System.out.println( "Hello World!" );
    }
}
